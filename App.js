import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './src/screens/Home/Home';
import FirstScreen from './src/screens/First/First';
import SecondScreen from './src/screens/Second/Second';
import ThirdScreen from './src/screens/Third/Third';
import SixthScreen from './src/screens/Sixth/Sixth';
import EighthScreen from './src/screens/Eighth/Eighth';
import NotificationScreen from './src/screens/Notification/Notification'
import FourthScreen from './src/screens/Fourth/Fourth';
import FifthScreen from './src/screens/Fifth/Fifth';
import { customFonts } from './fonts';
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';

const RootStack = createStackNavigator();
export default class App extends React.Component {

  state = {
    fontsLoaded: false,
  };

  async _loadFontsAsync() {
    await Font.loadAsync(customFonts);
    this.setState({ fontsLoaded: true });
  }

  componentDidMount() {
    this._loadFontsAsync();
  }

  render() {
    if (this.state.fontsLoaded) {
      return (
        <NavigationContainer>
          <RootStack.Navigator>
            <RootStack.Screen name="الأساسية" component={HomeScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="طلب جديد" component={FirstScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="خدمات" component={SecondScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="صيانه طارئه" component={ThirdScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="الأقسام والأصناف" component={FourthScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="توظيف" component={FifthScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="شكاوي" component={SixthScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="طلبات الشركات" component={EighthScreen} options={{ headerShown: false }} />
            <RootStack.Screen name="Notification" component={NotificationScreen} options={{ headerShown: false }} />
          </RootStack.Navigator>
        </NavigationContainer>
      );
    }
    else {
      return <AppLoading />;
    }
  }
}
