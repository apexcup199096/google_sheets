export const Theme = {
     colors: {
          primaryLight: '#4AA5DE',
          primaryDark:  '#2C4A90',

          gray:         '#959595',
          grayLight:    '#6D6D6D',
          grayDark:     '#444444',

          green: '#2bc3c3',
          
          background:   '#EDF1F4'
     }
}