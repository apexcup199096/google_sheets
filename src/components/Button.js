import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, ActivityIndicator } from 'react-native';
import { Theme } from '../../color';

export default class Button extends React.Component {
     render() {
          return (
               <View style={styles.container}>
                    <TouchableOpacity style={[styles.button, { backgroundColor: this.props.isDisable ? '#a1e6e6' : Theme.colors.green }]}
                         onPress={this.props.onPress}
                         activeOpacity={this.props.isDisable ? 1 : 0.5}
                    >
                         {
                              this.props.isDisable ?
                              <ActivityIndicator size="large" color={'#1e6d6d'} style={{display: 'flex'}} /> :
                              <Text style={styles.label}>{this.props.title}</Text>
                         }

                    </TouchableOpacity>
               </View>
          )
     }
}

const styles = StyleSheet.create({
     container: {
          flex: 1
     },
     button: {
          width: '100%',
          backgroundColor: Theme.colors.green,
          borderRadius: 5,
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: 50
     },
     label: {
          color: 'white',
          fontWeight: 'bold',
          fontSize: 18,
          fontFamily: 'arabic'
     }
})