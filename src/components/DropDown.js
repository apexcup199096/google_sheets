import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Platform } from 'react-native';
import { Theme } from '../../color';
import { Entypo } from '@expo/vector-icons';
import { ScrollView } from 'react-native-gesture-handler';

export default class DropDown extends React.Component {
     constructor(props) {
          super(props)
          this.state = {
               isExpend: false
          }
     }

     onClickDropDown = () => {
          this.setState({ isExpend: !this.state.isExpend })
     }

     render() {
          return (
               <View style={styles.container}>
                    <TouchableOpacity
                         onPress={this.onClickDropDown}
                         style={styles.dropDownContainer}
                    >
                         {
                              this.state.isExpend ?
                                   <Entypo name="chevron-thin-up" size={20} color={Theme.colors.primaryDark} /> :
                                   <Entypo name="chevron-thin-down" size={20} color={Theme.colors.primaryDark} />
                         }
                         <Text style={[styles.label, { color: this.props.selectedItem !== '' ? 'black' : 'gray' }]}>{this.props.selectedItem !== '' ? this.props.selectedItem : this.props.placeholder}</Text>
                    </TouchableOpacity>
                    {
                         this.state.isExpend && Platform.OS === 'android' ?
                              <ScrollView style={[styles.itemsContainer, { zIndex: 200 }]}>
                                   {
                                        this.props.options.map((item, index) => {
                                             return (
                                                  <TouchableOpacity style={styles.itemContainer} key={index}
                                                       onPress={() => {
                                                            this.setState({
                                                                 isExpend: false
                                                            })
                                                            this.props.selectItem(item.value)
                                                       }}
                                                  >
                                                       <Text style={styles.itemTextStyle}>{item.value}</Text>
                                                  </TouchableOpacity>
                                             )
                                        })
                                   }
                              </ScrollView> : null
                    }
                    {
                         this.state.isExpend && Platform.OS === 'ios' ?
                              <ScrollView style={[styles.itemsContainer]}>
                                   {
                                        this.props.options.map((item, index) => {
                                             return (
                                                  <TouchableOpacity style={styles.itemContainer} key={index}
                                                       onPress={() => {
                                                            this.setState({
                                                                 isExpend: false
                                                            })
                                                            this.props.selectItem(item.value)
                                                       }}
                                                  >
                                                       <Text style={styles.itemTextStyle}>{item.value}</Text>
                                                  </TouchableOpacity>
                                             )
                                        })
                                   }
                              </ScrollView> : null
                    }
               </View>
          )
     }
}

const styles = StyleSheet.create({
     container: {
          flex: 1,
          width: '100%',
     },
     dropDownContainer: {
          height: 40,
          backgroundColor: Theme.colors.background,
          display: 'flex',
          justifyContent: 'space-between',
          flexDirection: 'row',
          alignItems: 'center',
          paddingHorizontal: 10
     },
     itemsContainer: {
          display: 'flex',
          position: 'absolute',
          top: 40,
          backgroundColor: 'white',
          borderWidth: 1,
          borderColor: Theme.colors.background,
          width: '100%',
          height: 170,
          // zIndex: 200,
          // elevation: 200
     },
     itemContainer: {
          height: 40,
          width: '100%',
          backgroundColor: 'white',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'flex-end',
          paddingHorizontal: 10,
          borderBottomColor: Theme.colors.background,
          borderBottomWidth: 1,
          // zIndex: 200,
          // elevation: 200

     },
     itemTextStyle: {
          fontSize: 14,
          color: '#999',
          fontFamily: 'arabic'
     },
     label: {
          fontFamily: 'arabic'
     }
})