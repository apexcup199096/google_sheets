import React from 'react';
import { View, Text, StyleSheet, Image, TextInput, TouchableOpacity, Alert, Platform } from "react-native";
import Button from '../../components/Button';
import { Theme } from '../../../color';
import { backend_url } from '../../../server';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import InputScrollView from 'react-native-input-scroll-view';
import SearchableDropdown from 'react-native-searchable-dropdown';

export default class First extends React.Component {

     constructor(props) {
          super(props);
          this.state = {
               selectedCity: '',
               phone: '',
               token: '',
               date: new Date().getFullYear() + '/' + new Date().getDate() + '/' + Number(new Date().getMonth() + 1),
               time: new Date().getHours() + ':' + new Date().getMinutes(),
               isDisable: false,
               cities: this.props.route.params.cities
          }
     }

     _select = (value) => {
          this.setState({
               selectedCity: value.name
          })
     }

     _setPhone = (value) => {
          this.setState({
               phone: value
          })
     }

     _send = async () => {
          if (this.state.isDisable) return
          if (this.state.selectedCity === '') {
               Alert.alert(
                    "",
                    "الرجاء تحديد مدينتك",
                    [
                         { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
               )
               return
          }
          if (this.state.phone === '' || this.state.phone.length !== 10 || this.state.phone.substring(0, 2) !== '05') {
               Alert.alert(
                    "",
                    "الرجاء كتابة رقم الهاتف الصحيح",
                    [
                         { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
               )
               return
          }
          this.setState({ isDisable: true })
          const request = await fetch(`${backend_url}/first`,
               {
                    method: 'POST',
                    headers: {
                         Accept: 'application/json',
                         'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                         date: this.state.date,
                         time: this.state.time,
                         city: this.state.selectedCity,
                         mobile: this.state.phone,
                         employee: "",
                         case: "",
                         noticeable: "",
                         installation_date: "",
                         last_updated: "",
                         source: "",
                         appointment: ""
                    })
               }
          );
          if (request.status === 200) {
               this.setState({ isDisable: false })
               Alert.alert(
                    "",
                    "تم الارسال بنجاح، سيتم التواصل معك بأسرع وقت ممكن",
                    [
                         { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
               )
          } else {
               this.setState({ isDisable: false })
               Alert.alert(
                    "",
                    "فشل الحفظ ، يرجى المحاولة مرة أخرى",
                    [
                         { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
               )
          }
     }

     render() {
          const { colors } = Theme;
          return (
               <View style={styles.container}>
                    <LinearGradient
                         colors={[colors.primaryLight, colors.primaryDark]}
                         start={[0, 0]}
                         end={[1, 0]}
                         style={styles.header}
                    >
                         <TouchableOpacity
                              onPress={() => { this.props.navigation.goBack() }}
                         >
                              <Ionicons name="md-arrow-back" size={34} color="white" />
                         </TouchableOpacity>
                         <Image source={require('../../../assets/wlogo.png')}
                              style={{ height: 50, width: 100, resizeMode: 'stretch' }}
                         />
                    </LinearGradient>
                    <InputScrollView style={styles.body}
                         showsVerticalScrollIndicator={false}
                    >
                         <Text style={{ textAlign: 'right', width: '100%', fontFamily: 'arabic' }}>خلك مرتاح، عطنا جوالك وحنا بنتصل:</Text>
                         <View style={[styles.form, Platform.OS === 'ios' ? { zIndex: 200 } : null]}>
                              <Text style={styles.label}>المدينة:</Text>
                              <SearchableDropdown
                                   onItemSelect={this._select}
                                   containerStyle={{
                                        width: '100%',
                                   }}
                                   itemStyle={{
                                        padding: 10,
                                        backgroundColor: 'white',
                                        borderColor: '#E0E0E0',
                                        borderBottomWidth: 1,
                                        width: '100%',
                                        height: 45,
                                   }}
                                   itemTextStyle={{
                                        color: 'gray',
                                        width: '100%',
                                        textAlign: 'right',
                                        fontFamily: 'arabic'
                                   }}
                                   itemsContainerStyle={{
                                        width: '100%',
                                        height: 150,
                                        borderWidth: 1,
                                        borderColor: '#E0E0E0'
                                   }}
                                   items={this.state.cities}
                                   defaultIndex={0}
                                   resetValue={false}
                                   placeholder="اختر مدينتك ..."
                                   placeholderTextColor="gray"
                                   textInputStyle={{
                                        backgroundColor: Theme.colors.background,
                                        width: '100%',
                                        height: 40,
                                        fontFamily: 'arabic',
                                        textAlign: 'right',
                                        paddingHorizontal: 10
                                   }}
                                   listProps={{ nestedScrollEnabled: true }}
                              />
                         </View>
                         <View style={styles.form}>
                              <Text style={styles.label}>الجوال: (مثل 050XXXXXXX):</Text>
                              <TextInput
                                   maxLength={10}
                                   keyboardType='number-pad'
                                   onChangeText={this._setPhone}
                                   value={this.state.phone}
                                   style={styles.input}
                              />
                         </View>
                         <Text style={styles.description}>*خدمة الشحن متاحة لجميع مناطق المملكة</Text>
                         <Text style={styles.description}>*جميع الأسعار في الموقع تشمل قيمة الضريبة المضافة</Text>
                         <View style={{ width: '100%', marginTop: 30 }}>
                              <Button title="إرسال" onPress={this._send} isDisable={this.state.isDisable} />
                         </View>
                    </InputScrollView>
               </View>
          )
     }
}


const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#fff',
          width: '100%',
     },
     header: {
          height: 100,
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          paddingHorizontal: '5%',
          justifyContent: 'space-between',
          flexDirection: 'row',
          paddingTop: 20
     },
     body: {
          width: '100%',
          paddingHorizontal: '5%',
          paddingTop: 40,
     },
     form: {
          display: 'flex',
          flexDirection: 'column',
          width: '100%',
          alignItems: 'flex-end',
          marginVertical: 10,
     },
     label: {
          color: Theme.colors.primaryLight,
          fontFamily: 'arabic'
     },
     input: {
          backgroundColor: Theme.colors.background,
          width: '100%',
          height: 40,
          paddingHorizontal: 10,
          textAlign: 'right',
          fontFamily: 'arabic'
     },
     description: {
          fontSize: 10,
          textAlign: 'right',
          width: '100%',
          color: Theme.colors.gray,
          fontFamily: 'arabic'
     },
});
