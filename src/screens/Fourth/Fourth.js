import React from 'react';
import { View, Text, StyleSheet, TextInput, Image, TouchableOpacity, Dimensions } from "react-native";
import Button from '../../components/Button';
import { Theme } from '../../../color';
import InputScrollView from 'react-native-input-scroll-view';
import { backend_url } from '../../../server';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { CITY } from '../../utils/cities';
import DropDown from '../../components/DropDown';
import * as WebBrowser from 'expo-web-browser';

export default class Fourth extends React.Component {

     async componentDidMount() {
          await WebBrowser.openBrowserAsync('https://naqi.sa');
     }

     render() {
          const { colors } = Theme;

          return (
               <View style={styles.container}>
               </View>
          )
     }
}


const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#fff',
          width: '100%',
     },
     header: {
          height: 100,
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          paddingHorizontal: '5%',
          justifyContent: 'space-between',
          flexDirection: 'row',
          paddingTop: 20,
     },
     body: {
          width: '100%',
          paddingHorizontal: '5%',
          paddingTop: 40
     },
     form: {
          display: 'flex',
          flexDirection: 'column',
          width: '100%',
          alignItems: 'flex-end',
          marginVertical: 10
     },
     label: {
          color: Theme.colors.primaryLight,
     },
     input: {
          backgroundColor: Theme.colors.background,
          width: '100%',
          height: 40,
          paddingHorizontal: 10,
          textAlign: 'right'
     },
     description: {
          color: 'red',
          textAlign: 'right'
     }
});
