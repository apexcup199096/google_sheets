import React from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, Dimensions } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { Theme } from '../../../color';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import * as WebBrowser from 'expo-web-browser';
import { backend_url } from '../../../server';
import call from 'react-native-phone-call'
import { SliderBox } from "react-native-image-slider-box";
import ViewSlider from 'react-native-view-slider'
import Spinner from 'react-native-loading-spinner-overlay';
import * as SplashScreen from 'expo-splash-screen';
import AppLoading from 'expo-app-loading';

const screenWidth = Dimensions.get('screen').width;
const { width, height } = Dimensions.get('window');

export default class Home extends React.Component {
     constructor(props) {
          super(props)
          this.state = {
               cities: null,
               isReady: false,
               links: null
          }
     }

     async componentDidMount() {
          this.prepareResources();
     }

     prepareResources = async () => {
          try {
               await this.performAPICalls();
          } catch (e) {
               console.warn(e);
          } finally {
               this.setState({ isReady: true });
          }
     };

     performAPICalls = async () => {
          const request = await fetch(`${backend_url}/cities`,
               {
                    method: 'GET',
                    headers: {
                         Accept: 'application/json',
                         'Content-Type': 'application/json',
                    }
               }
          );
          let responseJson = await request.json();
          if (responseJson.ok) {
               this.setState({ cities: responseJson.cities })
          } else {
               console.log('error : ')
          }
     }

     openDrawer = () => {
          // this.props.navigation.openDrawer();
          this.props.navigation.navigate('Notification')
     }

     _goFirst = () => {
          this.props.navigation.navigate('طلب جديد', {
               cities: this.state.cities
          })
     }

     _goSecond = () => {
          this.props.navigation.navigate('خدمات', {
               cities: this.state.cities
          })
     }

     _goThird = () => {
          this.props.navigation.navigate('صيانه طارئه', {
               cities: this.state.cities
          })
     }
     _goWebstore = async () => {
          let result = await WebBrowser.openBrowserAsync('https://naqi.sa');
          return result;
     }
     _goFifth = async () => {
          let result = await WebBrowser.openBrowserAsync('https://naqi.sa/employment/');
          return result;
     }
     _goSixth = () => {
          this.props.navigation.navigate('شكاوي', {
               cities: this.state.cities
          })
     }
     _goEighth = () => {
          this.props.navigation.navigate('طلبات الشركات', {
               cities: this.state.cities
          })
     }
     _goContact = () => {
          const args = {
               number: '920021500',
               prompt: true
          }
          call(args).catch(console.error)
     }

     // _goSlider1 = async () => {
     //      try {
     //           await WebBrowser.openBrowserAsync(this.state.links[0].name);
     //      } catch (e) {
     //           console.log('error : ', e)
     //      }
     // }

     // _goSlider2 = async () => {
     //      try {
     //           await WebBrowser.openBrowserAsync(this.state.links[1].name);
     //      } catch (e) {
     //           console.log('error : ', e)
     //      }
     // }

     // _goSlider3 = async () => {
     //      try {
     //           await WebBrowser.openBrowserAsync(this.state.links[2].name);
     //      } catch (e) {
     //           console.log('error : ', e)
     //      }
     // }

     render() {
          const { colors } = Theme;
          // if (!this.state.isReady) {
          //      return null
          // }
          return (
               <View style={styles.container}>
                    <Spinner visible={!this.state.isReady} />
                    <LinearGradient
                         colors={[colors.primaryLight, colors.primaryDark]}
                         start={[0, 0]}
                         end={[1, 0]}
                         style={styles.header}
                    >
                         <TouchableOpacity
                              onPress={this.openDrawer}
                              style={{
                                   height: 60,
                                   display: 'flex',
                                   justifyContent: 'center',
                              }}
                         >
                              <Ionicons name="ios-notifications" size={26} color="white" style={{ marginTop: 5 }} />
                         </TouchableOpacity>
                         <Image source={require('../../../assets/wlogo.png')}
                              style={{ height: 50, width: 100, resizeMode: 'stretch' }}
                         />
                    </LinearGradient>
                    <View style={{ width: '100%' }}>
                         <ViewSlider
                              renderSlides={
                                   <>
                                        <View style={styles.viewBox}
                                        // onPress={this._goSlider1}
                                        >
                                             <Image source={require('../../../assets/img/slider1.jpg')} style={{ height: width / 3.15, width: width }} />
                                        </View>
                                        <View style={styles.viewBox}
                                        // onPress={this._goSlider2}
                                        >
                                             <Image source={require('../../../assets/img/slider2.jpg')} style={{ height: width / 3.15, width: width }} />
                                        </View>
                                        <View style={styles.viewBox}
                                        //  onPress={this._goSlider3}
                                        >
                                             <Image source={require('../../../assets/img/slider3.jpg')} style={{ height: width / 3.15, width: width }} />
                                        </View>
                                   </>
                              }
                              style={styles.slider}     //Main slider container style
                              height={width / 3.15}    //Height of your slider
                              slideCount={3}    //How many views you are adding to slide
                              dots={true}     // Pagination dots visibility true for visibile 
                              dotActiveColor='gray'     //Pagination dot active color
                              dotInactiveColor='#e0e0e0'    // Pagination do inactive color
                              dotsContainerStyle={styles.dotContainer}     // Container style of the pagination dots
                              autoSlide={true}    //The views will slide automatically
                              slideInterval={3000}    //In Miliseconds
                         />
                    </View>
                    <ScrollView style={styles.body}
                         showsVerticalScrollIndicator={false}
                    >
                         <TouchableOpacity
                              onPress={this._goFirst}
                         >
                              <LinearGradient
                                   colors={[colors.primaryLight, colors.primaryDark]}
                                   start={[0, 0]}
                                   end={[1, 0]}
                                   style={styles.first_form}
                              >
                                   <Image source={require('../../../assets/img/1.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                   <Text style={{ color: 'white', fontFamily: 'arabic' }}>طلب جديد</Text>
                              </LinearGradient>
                         </TouchableOpacity>
                         <View style={styles.second_row}>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goSecond}
                                   >
                                        <Image source={require('../../../assets/img/2.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>خدمات</Text>
                                   </TouchableOpacity>
                              </View>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goThird}
                                   >
                                        <Image source={require('../../../assets/img/3.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>صيانه طارئه</Text>
                                   </TouchableOpacity>
                              </View>
                         </View>
                         <View style={styles.second_row}>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goEighth}
                                   >
                                        <Image source={require('../../../assets/img/8.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>طلبات الشركات</Text>
                                   </TouchableOpacity>
                              </View>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goSixth}
                                   >
                                        <Image source={require('../../../assets/img/6.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>شكاوي</Text>
                                   </TouchableOpacity>
                              </View>
                         </View>
                         <TouchableOpacity
                              onPress={this._goWebstore}
                              style={{ marginVertical: 15 }}
                         >
                              <View style={styles.first_form}>
                                   <Image source={require('../../../assets/img/4.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                   <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>المتجر الالكتروني</Text>
                              </View>
                         </TouchableOpacity>
                         <View style={[styles.second_row, { marginBottom: 50, marginTop: 0 }]}>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goContact}
                                   >
                                        <Image source={require('../../../assets/img/7.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>اتصل بنا</Text>
                                   </TouchableOpacity>
                              </View>
                              <View style={styles.small_container}>
                                   <TouchableOpacity style={styles.small_button}
                                        onPress={this._goFifth}
                                   >
                                        <Image source={require('../../../assets/img/5.png')} width={40} height={40} style={{ width: 40, height: 40 }} />
                                        <Text style={{ marginTop: 10, fontFamily: 'arabic' }}>توظيف</Text>
                                   </TouchableOpacity>
                              </View>
                         </View>
                    </ScrollView>
               </View>
          )
     }
}


const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#fff',
          alignItems: 'center',
     },
     header: {
          height: 100,
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          paddingHorizontal: '5%',
          justifyContent: 'space-between',
          flexDirection: 'row',
          paddingTop: 10
     },
     body: {
          display: 'flex',
          width: '100%',
          alignSelf: 'center',
          paddingHorizontal: '5%',
          backgroundColor: Theme.colors.background,
          paddingVertical: 30,
     },
     first_form: {
          height: 100,
          width: '100%',
          borderRadius: 15,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          backgroundColor: 'white',
          shadowColor: "#939393",
          shadowOffset: {
               width: 1,
               height: 1,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
          elevation: 1,
     },
     second_row: {
          display: 'flex',
          width: '100%',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginTop: 15
     },
     small_container: {
          display: 'flex',
          width: '48%',
          backgroundColor: 'white',
          alignItems: 'center',
          justifyContent: 'center',
          borderRadius: 15,
          height: 100,
          shadowColor: "#939393",
          shadowOffset: {
               width: 1,
               height: 1,
          },
          shadowOpacity: 0.3,
          shadowRadius: 2,
          elevation: 1,
     },
     small_button: {
          width: '100%',
          height: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
     },
     viewBox: {
          justifyContent: 'center',
          width: width,
          alignItems: 'center',
          height: width / 3.15
     },
     slider: {
          alignSelf: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: Theme.colors.background,
     },
     dotContainer: {
          backgroundColor: 'transparent',
          position: 'absolute',
          bottom: -10
     }
});
