import React from 'react';
import { View, Image, StyleSheet } from "react-native";
import { Theme } from '../../../color';

export default class Loading extends React.Component {

     constructor(props) {
          super(props);
          this.state = {
          }
     }

     componentDidMount() {
          setTimeout(() => {
               this.props.navigation.navigate('الأساسية')
          }, 3000);
     }

     render() {
          const { colors } = Theme;
          return (
               <View style={styles.container}>
                   <Image source={require('../../../assets/splash.png')}
                         style={{resizeMode: 'stretch', width: '100%', height: '100%'}}
                   />
               </View>
          )
     }
}


const styles = StyleSheet.create({
     container: {
          flex: 1,
          backgroundColor: '#fff',
          alignItems: 'center',
          justifyContent: 'center'
     },
});
