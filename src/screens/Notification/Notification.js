import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
import * as Permissions from 'expo-permissions';
import React, { useState, useEffect, useRef } from 'react';
import { Text, View, Platform, Image, TouchableOpacity, Alert, StyleSheet, Keyboard, TouchableWithoutFeedback } from 'react-native';
import Button from '../../components/Button';
import { TextInput } from 'react-native-gesture-handler';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';
import { Theme } from '../../../color';
import InputScrollView from 'react-native-input-scroll-view';
import { Ionicons } from '@expo/vector-icons';

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: false,
    shouldSetBadge: false,
  }),
});

export default function App(props) {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [msg, setMsg] = useState('');
  const [title, setTitle] = useState('');
  const [notification, setNotification] = useState(false);
  const notificationListener = useRef();
  const responseListener = useRef();
  const [isDisable, setDisable] = useState(false)
  const { colors } = Theme

  useEffect(() => {
    registerForPushNotificationsAsync().then(token => setExpoPushToken(token));

    // This listener is fired whenever a notification is received while the app is foregrounded
    notificationListener.current = Notifications.addNotificationReceivedListener(notification => {
      setNotification(notification);
    });

    // This listener is fired whenever a user taps on or interacts with a notification (works when app is foregrounded, backgrounded, or killed)
    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
      console.log(response);
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener);
      Notifications.removeNotificationSubscription(responseListener);
    };
  }, []);

  function openDrawer() {
    props.navigation.goBack()
  }

  const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback
      onPress={() => Keyboard.dismiss()}> {children}
    </TouchableWithoutFeedback>
  );

  return (
    // <DismissKeyboard>
    <View style={styles.container}>
      <LinearGradient
        colors={[colors.primaryLight, colors.primaryDark]}
        start={[0, 0]}
        end={[1, 0]}
        style={styles.header}
      >
        <TouchableOpacity
          onPress={openDrawer}
        >
           <Ionicons name="md-arrow-back" size={34} color="white" />
        </TouchableOpacity>
        <Image source={require('../../../assets/wlogo.png')}
          style={{ height: 50, width: 100, resizeMode: 'stretch' }}
        />
      </LinearGradient>
      <InputScrollView style={styles.body}>
        <View style={{width: '100%'}}>
          <View style={{ width: '100%' }}>
            <View>
              <Text style={{ marginBottom: 5, textAlign: 'right' }}>عنوان</Text>
              <TextInput style={{ backgroundColor: Theme.colors.background, height: 38, paddingHorizontal: 10, borderRadius: 5, textAlign: 'right' }}
                value={title}
                onChangeText={(value) => { setTitle(value) }}
              />
            </View>
            <View style={{ marginTop: 20 }}>
              <Text style={{ marginBottom: 5, textAlign: 'right' }}>رسالة</Text>
              <TextInput style={{ backgroundColor: Theme.colors.background, height: 100, paddingHorizontal: 10, borderRadius: 5, textAlign: 'right', textAlignVertical: 'top', paddingTop: 10 }}
                value={msg}
                multiline
                numberOfLines={5}
                onChangeText={(value) => { setMsg(value) }}
              />
            </View>
          </View>
          <View style={{ width: '100%', alignSelf: 'center', marginTop: 70 }}>
            <Button title="إرسال"
              onPress={async () => {
                if (msg === '' || title === '') {
                  Alert.alert(
                    "",
                    "لو سمحت أملأ كل الحقول",
                    [
                         { text: "OK", onPress: () => console.log("OK Pressed") }
                    ],
                    { cancelable: false }
                  )
                  return
                }
                setDisable(true)
                await sendPushNotification(expoPushToken, title, msg);
                setDisable(false)
              }}
              isDisable={isDisable}
            />
          </View>
        </View>
      </InputScrollView>

    </View>
    // {/* </DismissKeyboard> */}
  );
}

// Can use this function below, OR use Expo's Push Notification Tool-> https://expo.io/notifications
async function sendPushNotification(expoPushToken, title, msg) {
  const message = {
    to: expoPushToken,
    sound: 'default',
    title: title,
    body: msg,
    data: { data: 'goes here' },
  };

  await fetch('https://exp.host/--/api/v2/push/send', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Accept-encoding': 'gzip, deflate',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(message),
  });
}

async function registerForPushNotificationsAsync() {
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    if (existingStatus !== 'granted') {
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      Alert.alert(
        "",
        "Failed to get push token for push notification!",
        [
             { text: "OK", onPress: () => console.log("OK Pressed") }
        ],
        { cancelable: false }
   )
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    console.log(token);
  } else {
    Alert.alert(
      "",
      "Must use physical device for Push Notifications",
      [
           { text: "OK", onPress: () => console.log("OK Pressed") }
      ],
      { cancelable: false }
 )
  }

  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    width: '100%'
  },
  body: {
    width: '100%',
    paddingHorizontal: '5%',
    paddingTop: 40
},
  header: {
    height: 100,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    paddingHorizontal: '5%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop: 10,
  },
});
