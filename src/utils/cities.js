export const CITY = [
     {
          label: 'Juventus',
          value: 'JUVE',
     },
     {
          label: 'Real Madrid',
          value: 'RM',
     },
     {
          label: 'Barcelona',
          value: 'BR',
     },
     {
          label: 'PSG',
          value: 'PSG',
     },
     {
          label: 'FC Bayern Munich',
          value: 'FBM',
     },
]
